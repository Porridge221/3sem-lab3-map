#include <iostream>
#include "Map.hpp"

int main() {

    Map<int, int> a;

    a.InsertElement(2, 20);
    a.InsertElement(1, 10);
    a.InsertElement(4, 40);
    a.InsertElement(3, 30);
    a.InsertElement(5, 50);

    std::cout << a.getSize() << std::endl;
    std::cout << "Hello, World!" << std::endl;
    return 0;
}