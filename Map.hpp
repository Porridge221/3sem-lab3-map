//
// Created by yuri on 07.03.17.
//

#ifndef INC_3SEM_LAB3_MAP_MAP_HPP
#define INC_3SEM_LAB3_MAP_MAP_HPP

#include <algorithm>
#include <utility>

template <typename Key, typename T>
class Map {
public:
    using key_type = Key;
    using mapped_type = T;
    using value_type = std::pair<key_type, mapped_type>;

    enum IteratorTypeT {
        ITERATOR_DEREFERENCABLE,
        ITERATOR_BEFORE_FIRST,
        ITERATOR_PAST_REAR,
    };
    struct IteratorT;

private:
    struct Node;

    int DEBUG;
    Node *root;
    size_t size;


    struct Node {

        Node(key_type key_, mapped_type value_, Node *parent_) {
            key = key_;
            value = value_;
            height = 0;
            parent = parent_;
            left = nullptr;
            right = nullptr;
        }

        int GetNodeHeight() {
            return this == nullptr ? 0 : height;
        }

        int NodeBalanceParameter() {
            return left->GetNodeHeight() - right->GetNodeHeight();
        }

        void RefreshNodeHeight() {
            height = 1 + std::max(left->GetNodeHeight(), right->GetNodeHeight());
        }

        Node *GetLeftLeaf() {
            while (left != nullptr)
                this = left;

            return this;
        }

        Node *GetRightLeaf() {
            while (right != nullptr)
                this = right;

            return this;
        }

        Node *GetNodeByKey(key_type key_) {
            while (this != nullptr && key != key_)
                if (key < key_)
                    this = right;
                else
                    this = left;

            return this;
        }

        Node *GoToLeaf(key_type key_) {
            if (key_ < key) {
                if (left != nullptr)
                    return left->GoToLeaf(key_);
                else
                    return this;
            }
            else
            if (key_ > key) {
                if (right != nullptr)
                    return right->GoToLeaf(key_);
                else
                    return this;
            }
            else
                return this;
        }

        struct Node* left;
        struct Node* right;
        struct Node* parent;
        mapped_type value;
        key_type key;

        int height;
    };
    /////////////////////////


    void PeplaceNode(Node *node, Node *newNode) {
        if (newNode != nullptr)
            newNode->parent = node->parent;

        if (node->parent == nullptr)
            this->root = newNode;
        else
        if (node->parent->left == node)
            node->parent->left = newNode;
        else
            node->parent->right = newNode;
    }

    void LeftRotate(Node *node) {
        Node *newRoot = node->right;
        PeplaceNode(node, newRoot);
        node->right = newRoot->left;
        if (node->right != nullptr)
            node->right->parent = node;
        node->parent = newRoot;
        newRoot->left = node;
        node->RefreshNodeHeight();
        newRoot->RefreshNodeHeight();
    }

    void RightRotate(Node *node) {
        Node *newRoot = node->left;
        PeplaceNode(node, newRoot);
        node->left = newRoot->right;
        if (node->left != nullptr)
            node->left->parent = node;
        node->parent = newRoot;
        newRoot->right = node;
        node->RefreshNodeHeight();
        node->RefreshNodeHeight();
    }

    void Balance(Node *node) {
        Node *parent = nullptr;
        int nodeBalance;
        while (node != nullptr) {

            node->RefreshNodeHeight();
            nodeBalance = node->NodeBalanceParameter();
            parent = node->parent;
            if (abs(nodeBalance) <= 1)
                return;
            else
            if (nodeBalance < -1) {
                if (node->right->NodeBalanceParameter() > 0)
                    this->RightRotate(node->right);
                this->LeftRotate(node);
            }
            else
            if (nodeBalance > 1) {
                if (node->left->NodeBalanceParameter() < 0)
                    this->LeftRotate(node->left);
                this->RightRotate(node);
            }
            node = parent;
        }
    }

public:

    struct IteratorT {
    private:
        IteratorTypeT type;
        Map *tree;
        Node *node;

    public:
        IteratorT(Map *handle_, Node *node_, IteratorTypeT type_) {
            node = node_;
            tree = handle_;
            type = type_;
        }

        int IsIteratorDereferencable() {
            return (this != nullptr && !this->IsIteratorBeforeFirst() && !this->IsIteratorPastRear());
        }

        int IsIteratorPastRear() {
            return (this != nullptr && type == ITERATOR_PAST_REAR);
        }

        int IsIteratorBeforeFirst() {
            return (this != nullptr && type == ITERATOR_BEFORE_FIRST);
        }

        mapped_type *DereferenceIterator() {
            if (this == nullptr || node == nullptr)
                return nullptr;
            return &(node->value);
        }

        key_type GetIteratorKey() {
            if (this == nullptr || node == nullptr)
                return -1;

            return node->key;
        }

        void AdvanceOneElement() {
            IteratorT *it = this;

            if (it == nullptr || it->type == ITERATOR_PAST_REAR)
                return;

            if (it->type == ITERATOR_BEFORE_FIRST) {
                if (it->tree->root == nullptr)
                    it->type = ITERATOR_PAST_REAR;
                else {
                    it->node = it->tree->root->GetLeftLeaf();
                    it->type = ITERATOR_DEREFERENCABLE;
                }
                return;
            }

            Node *node = it->node;
            Node *leafnode = nullptr;

            while (node->parent != nullptr && node->right == leafnode) {
                leafnode = node;
                node = node->parent;
            }

            if (node->right == leafnode) {
                it->type = ITERATOR_PAST_REAR;
                it->node = nullptr;
            }
            else
            if (node != it->node)
                it->node = node;
            else
            if (node->right != nullptr)
                it->node = node->right->GetLeftLeaf();

        }

        void RewindOneElement() {
            IteratorT *it = this;

            if (it == nullptr || it->type == ITERATOR_BEFORE_FIRST)
                return;

            if (it->type == ITERATOR_PAST_REAR) {
                if (it->tree->root == nullptr)
                    it->type = ITERATOR_BEFORE_FIRST;
                else {
                    it->node = it->tree->root->GetLeftLeaf();
                    it->type = ITERATOR_DEREFERENCABLE;
                }
                return;
            }

            Node *node = it->node;
            Node *parent = nullptr;

            while (node->parent != nullptr && node->left == parent) {
                parent = node;
                node = node->parent;
            }

            if (node->left == parent) {
                it->type = ITERATOR_BEFORE_FIRST;
                it->node = nullptr;
            }
            else
            if (node != it->node)
                it->node = node;
            else
            if (node->left != nullptr)
                it->node = node->left->GetRightLeaf();

        }

        void ShiftPosition(size_t shift) {
            if (this == nullptr)
                return;

            for (; shift > 0; this->AdvanceOneElement(), shift--);
            for (; shift < 0; this->RewindOneElement(), shift++);
        }

        void SetPosition(size_t pos) {
            if (this == nullptr)
                return;

            type = ITERATOR_BEFORE_FIRST;
            this->ShiftPosition(pos + 1);
        }

    };

    Map() {
        root = nullptr;
        size = 0;
    }

    //extern void LSQ_DestroySequence(LSQ_HandleT handle) {
    //    FreeNode(((TreePtrT)handle)->root);
    //    free(handle);
    //}


    size_t getSize() {
        return (this != nullptr ? size : 0);
    }


    IteratorT GetElementByIndex(key_type index) {
        if (this == nullptr)
            return nullptr;

        Node *node = this->root->GetNodeByKey(index);

        if (node == nullptr)
            return this->GetPastRearElement();

        return IteratorT(this, node, ITERATOR_DEREFERENCABLE);
    }

    IteratorT GetFrontElement() {
        if (this == nullptr)
            return nullptr;

        IteratorT it(this, nullptr, ITERATOR_BEFORE_FIRST);

        if (&it == nullptr)
            return nullptr;

        it.AdvanceOneElement();

        return it;
    }

    IteratorT GetPastRearElement() {
        IteratorT it(this, nullptr, ITERATOR_PAST_REAR);
        return it;
    }


    //extern void LSQ_DestroyIterator(LSQ_IteratorT iterator) {
    //    free(iterator);
    //}

    void InsertElement(key_type key, mapped_type value) {
        if (this == nullptr)
            return;

        Map *tree = this;

        if (tree->root == nullptr) {
            tree->root = new Node(key, value, nullptr);
            if (tree->root == nullptr)
                return;
            tree->size++;
        }

        Node *parent = tree->root->GoToLeaf(key);
        if (parent->key == key) {
            parent->value = value;
            return;
        }

        Node *node = new Node(key, value, parent);
        if (node == nullptr)
            return;

        tree->size++;
        if (key < parent->key)
            parent->left = node;
        else
            parent->right = node;

        tree->Balance(parent);
    }

    void DeleteFrontElement() {
        IteratorT it = this->GetFrontElement();
        if (this == nullptr || &it == nullptr)
            return;
        this->DeleteElement(it.GetIteratorKey());
        //it.DestroyIterator();
    }

    void DeleteRearElement() {
        IteratorT it = this->GetPastRearElement();
        if (this == nullptr || it == nullptr)
            return;
        it.RewindOneElement();
        this->DeleteElement(it.GetIteratorKey());
        //it.DestroyIterator();
    }

    void DeleteElement(key_type key) {
        Map *tree = this;

        if (this == nullptr || tree->root == nullptr)
            return;

        Node *node = tree->root->GetNodeByKey(key);
        if (node == nullptr)
            return;

        Node *parent = node->parent;
        Node *left = nullptr;
        int keyValue;

        if (node->left == nullptr && node->right == nullptr)
            tree->PeplaceNode(node, nullptr);
        else
        if (node->left != nullptr && node->right != nullptr) {
            left = node->right->GetLeftLeaf();
            keyValue = left->key;
            node->value = left->value;
            this->DeleteElement(left->key);
            node->key = keyValue;
            return;
        }
        else
        if (node->left != nullptr)
            tree->PeplaceNode(node, node->left);
        else
        if (node->right != nullptr)
            tree->PeplaceNode(node, node->right);

        free(node);
        tree->size--;
        tree->Balance(parent);
    }

};


#endif //INC_3SEM_LAB3_MAP_MAP_HPP
